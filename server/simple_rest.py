import cgi
import json

import model


def not_found_404(environ, start_response):
    start_response('404 not found', [('Content-type', 'text/plain')])
    return [b'Not found']


class PathDispatcher(object):
    def __init__(self):
        self.pathmap = {}

    def __call__(self, environ, start_response):
        path = environ['PATH_INFO']
        params = cgi.FieldStorage(
            environ['wsgi.input'],
            environ=environ
        )
        method = environ['REQUEST_METHOD'].lower()
        environ['params'] = {key: params.getvalue(key) for key in params}
        handler = self.pathmap.get((method, path), not_found_404)
        return handler(environ, start_response)

    def register(self, method, path, function):
        self.pathmap[method.lower(), path] = function
        return function


def ping(environ, start_response):
    start_response("200 OK", [('Content-Type', 'text/plain')])
    params = environ['params']
    resp = 'Server is running.'
    if params:
        resp += ' Sent params: %s' % str(params)
    return [resp.encode('utf-8')]


def users(environ, start_response):
    start_response("200 OK", [('Content-Type', 'application/json')])
    with model.session_scope() as db_session:
        resp = []
        for user in db_session.query(model.User).all():
            resp.append({
                'username': user.username,
                'fullname': user.fullname,
                'email': user.email,
            })
        return [json.dumps(resp).encode('utf-8')]