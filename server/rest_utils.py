import base64
import flask
import hashlib
import httplib
from flask import request
import server.model as model
import bin.cli_client


def parse_authorization_header():
    if 'Authorization' not in request.headers:
        return None, None
    kind, data = request.headers['Authorization'].split()
    if kind.upper() != 'BASIC':
        return None, None
    credentials = base64.decodestring(data)
    username, _, password = credentials.partition(':')
    return username, password


def get_request_user():
    """" Method return current request user. On unknown user returns None """
    username, _ = parse_authorization_header()
    if username is None:
        return None
    try:
        with model.session_scope() as db_session:
            user = db_session.query(model.User).filter_by(
                username=username
            ).one()
            db_session.expunge(user)
            return user
    except Exception as e:
        print e.message
        return None


def authenticate_user():
    username, password = parse_authorization_header()
    if username is None:
        flask.abort(httplib.UNAUTHORIZED)
    try:
        with model.session_scope() as db_session:
            user = db_session.query(model.User).filter_by(
                username=username
            ).one()
            if not check_credentials(user, password, plain=False):
                flask.abort(httplib.UNAUTHORIZED)
    except Exception:
        flask.abort(httplib.UNAUTHORIZED)


def check_credentials(user, password, plain=False):
    if plain:
        password = hashlib.sha256(password).hexdigest()
    if not user.check_password(hashed=password):
        return False
    return True


def get_rest_client(user, address='http://127.0.0.1:8080'):
    client = bin.cli_client.Client(standalone=False)
    client.user = user.username
    client.password = user.password
    client.address = address
    return client
