import os
import hashlib
import base64
import flask
import server.model as model
import httplib
import datetime
import dateutil

from flask import request
from functools import wraps
from rest_utils import get_request_user, authenticate_user as auth_user

app = flask.Flask(__name__)


def authenticate_user(func):
    """" Decorator that authenticates user """
    @wraps(func)
    def wrapper(*args, **kwargs):
        auth_user()
        return func(*args, **kwargs)
    return wrapper


def admin_required(func):
    """" Decorator that verify that authenticated user has admin access  """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            user = get_request_user()
            if user is None:
                flask.abort(httplib.UNAUTHORIZED)
            if not user.is_admin:
                flask.abort(httplib.UNAUTHORIZED)
        except Exception as e:
            print e.message
            flask.abort(httplib.UNAUTHORIZED)
        return func(*args, **kwargs)
    return wrapper


def premium_required(func):
    """" Decorator that verify that authenticated user has premium access  """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            user = get_request_user()
            if user is None:
                flask.abort(httplib.UNAUTHORIZED)
            if not user.is_premium:
                flask.abort(httplib.UNAUTHORIZED)
        except Exception as e:
            print e.message
            flask.abort(httplib.UNAUTHORIZED)
        return func(*args, **kwargs)
    return wrapper


def self_or_admin(user_id):
    request_user = get_request_user()
    if request_user.id != int(user_id) or not request_user.is_admin:
        flask.abort(httplib.FORBIDDEN)


@app.route('/')
def ping():
    return 'Flask based API is running.'


@app.route('/api/activity_types', methods=['GET'])
@authenticate_user
def activity_types():
    with model.session_scope() as db_session:
        resp = []
        for activity_type in db_session.query(model.ActivityType).all():
            resp.append(activity_type.to_dict())
        return flask.jsonify(resp)


@app.route('/api/users')
@admin_required
def users():
    with model.session_scope() as db_session:
        resp = []
        for user in db_session.query(model.User).all():
            resp.append({
                'username': user.username,
                'fullname': user.fullname,
                'email': user.email,
            })
        return flask.jsonify(resp)


@app.route('/api/users/<user_name>')
def user_info(user_name):
    with model.session_scope() as db_session:
        user = db_session.query(model.User).filter_by(username=user_name).one()
        self_or_admin(user.id)
        return flask.jsonify(user.to_dict())


@app.route('/api/users', methods=['PUT'])
@authenticate_user
@admin_required
def create_user():
    if not request.json:
        flask.abort(400)
    try:
        with model.session_scope() as db_session:
            user = model.User(
                username=request.json['username'],
                fullname=request.json['fullname'],
                email=request.json['email'],
                account_type=request.json['account_type'],
            )
            user.password = request.json['password']
            db_session.add(user)
            return flask.jsonify({'user': user.to_dict()}), 201
    except Exception as e:
        flask.abort(e)


@app.route('/api/users/<user_id>', methods=['DELETE'])
@authenticate_user
@admin_required
def delete_user(user_id):
    try:
        with model.session_scope() as db_session:
            user = db_session.query(model.User).get(user_id)
            db_session.delete(user)
            return flask.jsonify({'result': True})
    except Exception as e:
        flask.abort(e)


@app.route('/api/users/<user_id>', methods=['POST'])
@authenticate_user
def update_user(user_id):
    self_or_admin(user_id)
    if not request.json:
        flask.abort(400)
    try:
        with model.session_scope() as db_session:
            user = db_session.query(model.User).get(user_id)
            if 'fullname' in request.json:
                user.fullname = request.json['fullname']
            if 'email' in request.json:
                user.email = request.json['email']
            if 'password' in request.json:
                user.password = request.json['password']
            if 'account_type' in request.json:
                user.password = request.json['account_type']
            return flask.jsonify({'user': user.to_dict()}), 201
    except Exception as e:
        flask.abort(e)


@app.route('/api/user_activity/<user_id>', methods=['GET'])
@authenticate_user
def user_activity(user_id):
    self_or_admin(user_id)
    try:
        with model.session_scope() as db_session:
            activities = db_session.query(model.Activity).filter_by(user_id=user_id).all()
            result = {}
            for activity in activities:
                result[activity.id] = activity.to_dict(skip_laps=True)
            return flask.jsonify(result)
    except Exception as e:
        flask.abort(e)


@app.route('/api/activity/<activity_id>', methods=['GET'])
@authenticate_user
def get_activity(activity_id):
    try:
        with model.session_scope() as db_session:
            activity = db_session.query(model.Activity).get(activity_id)
            return flask.jsonify(activity.to_dict())
    except Exception as e:
        flask.abort(e)


@app.route('/api/activity/', methods=['PUT'])
@authenticate_user
def create_activity():
    if not request.json:
        flask.abort(400)
    try:
        user = get_request_user()
        with model.session_scope() as db_session:
            activity_type = db_session.query(model.ActivityType).get(
                request.json['activity_type_id']
            )
            activity = model.Activity(
                user=user,
                name=request.json['name'],
                description=request.json['description'],
                start=dateutil.parser.parser().parse(request.json['start']),
                end=dateutil.parser.parser().parse(request.json['end']),
                activity_type=activity_type,
                laps=[],
            )
            for lap_index, lap_data in enumerate(request.json['laps']):
                lap = model.Lap.from_dict(lap_index=lap_index, lap_data=lap_data)
                activity.laps.append(lap)
                db_session.add(activity)
            return flask.jsonify(activity.to_dict()), 201
    except Exception as e:
        flask.abort(e)


@app.route('/api/activity/<activity_id>', methods=['DELETE'])
@authenticate_user
def delete_activity(activity_id):
    try:

        with model.session_scope() as db_session:
            activity = db_session.query(model.Activity).get(activity_id)
            self_or_admin(activity.user_id)
            user = get_request_user()
            if user.id != activity.user_id:
                flask.abort(httplib.FORBIDDEN)
            db_session.delete(activity)
            return flask.jsonify({'result': True})
    except Exception as e:
        flask.abort(e)


@app.route('/api/activity/<activity_id>', methods=['POST'])
@authenticate_user
def update_activity(activity_id):
    if not request.json:
        flask.abort(400)
    try:
        with model.session_scope() as db_session:
            activity = db_session.query(model.Activity).get(activity_id)
            self_or_admin(activity.user_id)
            if 'name' in request.json:
                activity.name = request.json['name']
            if 'description' in request.json:
                activity.description = request.json['description']
            if 'start' in request.json:
                activity.start = datetime.datetime.fromtimestamp(request.json['start'])
            if 'end' in request.json:
                activity.end = datetime.datetime.fromtimestamp(request.json['end'])
            if 'activity_type' in request.json:
                activity_type = db_session.query(model.ActivityType).get(request.json['activity_type'])
                activity.activity_type_id = activity_type.id
            if 'curr_lap_append' in request.json:
                lap_data = request.json['curr_lap_append']
                last_lap = activity.current_lap
                index = last_lap.index
                for data in lap_data:
                    index += 1
                    point = model.Point.from_dict(point_index=index, point_data=data)
                    last_lap.append(point)
            if 'lap_append' in request.json:
                laps_data = request.json['lap_append']
                last_lap = activity.current_lap
                index = last_lap.index
                for data in laps_data:
                    index += 1
                    lap = model.Lap.from_dict(point_index=index, lap_data=data)
                    activity.laps.append[lap]
            return flask.jsonify({'activity': activity.to_dict()}), 201
    except Exception as e:
        flask.abort(e)
