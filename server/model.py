import enum
import hashlib
import datetime
from contextlib import contextmanager
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, Float, String, DateTime, Enum, Boolean
from sqlalchemy.schema import ForeignKey
from sqlalchemy.orm import relationship

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, backref

Base = declarative_base()


class AccountTypes(enum.Enum):
    admin = 'admin'
    member = 'member'
    premium_member = 'premium_member'


@contextmanager
def session_scope():
    session = Database.get_session()
    try:
        yield session
    except Exception as e:
        session.rollback()
        raise e
    else:
        session.commit()
    finally:
        session.close()


class Database(object):
    engine = None
    session = None

    @classmethod
    def init_db(cls, path, drop_all=False, create_all=True):
        cls.engine = create_engine(path)
        if drop_all:
            Base.metadata.drop_all(cls.engine)
        if create_all:
            Base.metadata.create_all(cls.engine)
        cls.session = sessionmaker(bind=cls.engine)

    @classmethod
    def get_session(cls):
        return cls.session()


followers = Table(
    'followers', Base.metadata,
    Column('follower_id', Integer, ForeignKey('users.id')),
    Column('followed_id', Integer, ForeignKey('users.id'))
)


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(50), unique=True)
    fullname = Column(String(100))
    email = Column(String(100), unique=True)
    _password = Column(String(150))
    account_type = Column(Enum(AccountTypes))
    activities = relationship("Activity", lazy='subquery')
    followed = relationship(
        'User',
        secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=backref('followers', lazy='dynamic'),
        lazy='dynamic'
    )
    share_live_data = Column(Boolean, default=False)

    def __init__(self, username, fullname, email, account_type=AccountTypes.member):
        self.username = username
        self.fullname = fullname
        self.email = email
        self.authenticated = False
        self._password = None
        self.account_type = account_type

    @property
    def has_live_activity(self):
        result = []
        for activity in self.activities:
            if activity.is_live:
                result.append(activity)
        return result

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, value):
        self._password = hashlib.sha256(value).hexdigest()

    def check_password(self, hashed):
        # Todo : eliminate side channel info leaking
        return self.password == hashed

    @property
    def is_admin(self):
        return self.account_type == AccountTypes.admin

    @property
    def is_premium(self):
        return self.account_type == AccountTypes.premium_member

    def to_dict(self):
        return {
            'username': self.username,
            'fullname': self.fullname,
            'email': self.email,
            'account_type': self.account_type.value,
        }

    # required by Flask login
    @property
    def is_authenticated(self):
        return True

    # required by Flask login
    @property
    def is_active(self):
        return True

    # required by Flask login
    @property
    def is_anonymous(self):
        return False

    # required by Flask login
    def get_id(self):
        return unicode(self.id)

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)
            return self

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)
            return self

    def is_following(self, user):
        return self.followed.filter(followers.c.followed_id == user.id).count() > 0


class ActivityType(Base):
    __tablename__ = 'activity_types'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), unique=True)

    def __init__(self, name):
        self.name = name

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
        }


class Point(Base):
    __tablename__ = 'points'

    # TODO: uuid
    id = Column(Integer, primary_key=True, autoincrement=True)
    index = Column(Integer)
    lap_id = Column(Integer, ForeignKey('laps.id'))
    lat = Column(Float)
    long = Column(Float)
    elev = Column(Integer)
    speed = Column(Float)
    distance = Column(Float)
    heart_rate = Column(Integer)
    cadence = Column(Integer)
    watts = Column(Integer)
    training_time = Column(Float)
    training_time_absolute = Column(Float)

    def __init__(
            self, index, lat=None, long=None, elev=None, speed=None, distance=None,
            heart_rate=None, cadence=None, watts=None,
            training_time=None, training_time_absolute=None,):
        self.index = index
        self.lat = lat
        self.long = long
        self.elev = elev
        self.speed = speed
        self.distance = distance
        self.heart_rate = heart_rate
        self.cadence = cadence
        self.watts = watts
        self.training_time = training_time
        self.training_time_absolute = training_time_absolute

    def __eq__(self, other):
        return self.id == other.id

    def to_dict(self):
        return {
            'activity_id': self.lap_id,
            'index': self.index,
            'lat': self.lat,
            'long': self.long,
            'elev': self.elev,
            'speed': self.speed,
            'distance': self.distance,
            'heart_rate': self.heart_rate,
            'cadence': self.cadence,
            'watts': self.watts,
            'training_time': self.training_time,
            'training_time_absolute': self.training_time_absolute,
        }

    @staticmethod
    def from_dict(point_index, point_data):
        point = Point(
            index=point_index,
            lat=point_data['lat'],
            long=point_data['long'],
            elev=point_data['elev'],
            speed=point_data['speed'],
            distance=point_data['distance'],
            heart_rate=point_data['heart_rate'],
            cadence=point_data['cadence'],
            watts=point_data['watts'],
            training_time=point_data['training_time'],
            training_time_absolute=point_data['training_time_absolute'],
        )
        return point


class Lap(Base):
    __tablename__ = 'laps'

    id = Column(Integer, primary_key=True, autoincrement=True)
    index = Column(Integer)
    activity_id = Column(Integer, ForeignKey('activities.id'))
    points = relationship('Point', order_by=Point.index)

    seconds = Column(Float)
    distance = Column(Float)
    max_speed = Column(Float)
    calories = Column(Integer)
    avg_heart_rate = Column(Integer)
    max_heart_rate = Column(Integer)
    cadence = Column(Integer)

    def __init__(
            self, index, points, seconds=0, distance=0, max_speed=0, calories=0,
            avg_heart_rate=0, max_heart_rate=0, cadence=0):
        self.index = index
        self.points = points
        self.seconds = seconds
        self.distance = distance
        self.max_speed = max_speed
        self.calories = calories
        self.avg_heart_rate = avg_heart_rate
        self.max_heart_rate = max_heart_rate
        self.cadence = cadence

    def to_dict(self, raw=False):
        return {
            'id': self.id,
            'index': self.index,
            'activity_id': self.activity_id,
            'seconds': self.seconds,
            'distance': self.points[-1].distance,
            'max_speed': self.max_speed,
            'calories': self.calories,
            'avg_heart_rate': self.avg_heart_rate,
            'max_heart_rate': self.max_heart_rate,
            'cadence': self.cadence,
            'points': [p.to_dict() for p in self.points],
        }

    @staticmethod
    def from_dict(lap_index, lap_data):
        lap = Lap(index=lap_index, points=[])

        lap.seconds = lap_data['seconds']
        lap.distance = lap_data['distance']
        lap.max_speed = lap_data['max_speed']
        lap.calories = lap_data['calories']
        lap.avg_heart_rate = lap_data['avg_heart_rate']
        lap.max_heart_rate = lap_data['max_heart_rate']
        lap.cadence = lap_data['cadence']

        for point_index, point_data in enumerate(lap_data['points']):
            point = Point.from_dict(
                point_index=point_index,
                point_data=point_data
            )
            lap.points.append(point)
        return lap


class Activity(Base):
    __tablename__ = 'activities'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(160))
    description = Column(String(1000))
    start = Column(DateTime)
    end = Column(DateTime)
    activity_type_id = Column(Integer, ForeignKey('activity_types.id'))
    activity_type = relationship('ActivityType', order_by=ActivityType.id)
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship('User', order_by=User.id)
    laps = relationship('Lap', order_by=Lap.index)
    is_live = Column(Boolean, default=False)

    def __init__(self, user, name, description, start, end, activity_type, laps, is_live=False):
        self.user_id = user.id
        self.user = user
        self.name = name
        self.description = description
        self.start = start
        self.end = end
        self.activity_type_id = activity_type.id
        self.activity_type = activity_type
        self.laps = laps

    @property
    def distance(self):
        return float(self.laps[-1].points[-1].distance)

    @property
    def training_time(self):
        return float(self.laps[-1].points[-1].training_time)

    @property
    def training_time_absolute(self):
        return float(self.laps[-1].points[-1].training_time_absolute)

    @property
    def altitude_ascend_descend(self):
        ascend = 0
        descend = 0
        current = self.laps[0].points[0].elev
        for lap in self.laps:
            for point in lap.points:
                if point.elev > current:
                    ascend += point.elev - current
                if point.elev < current:
                    descend += current - point.elev
                current = point.elev
        return ascend, descend

    @property
    def current_lap(self):
        if not self.laps:
            lap = Lap(index=0)
            self.laps.append(lap)
            return lap
        else:
            return self.laps[-1]

    def to_dict(self, skip_laps=False, raw=False):
        ascend, descend = self.altitude_ascend_descend
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'start': str(self.start) if raw else self.start.strftime("%A %d. %B %Y"),
            'end': str(self.end) if raw else self.end.strftime("%A %d. %B %Y"),
            'distance': self.distance if raw else "%.2f %s" % ((self.distance / 1000), 'km'),
            'duration': self.training_time_absolute if raw else str(datetime.timedelta(seconds=self.training_time_absolute)),
            'ascend': ascend,
            'descend': descend,
            'activity_type': self.activity_type.name,
            'activity_type_id': self.activity_type_id,
            'is_live': self.is_live,
            'laps': [] if skip_laps else [p.to_dict(raw=raw) for p in self.laps],
        }
