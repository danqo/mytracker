import xml
import xml.dom
import server.model
import dateutil.parser
#import datetime.timedelta
import datetime

class Format(object):

    def __init__(self, io, user):
        self.user = user
        self._parse(io)

    def _parse(self, io):
        pass

    def build_activity(self):
        pass


class SigmaFileFormat(Format):

    def _parse(self, io):
        pass

    def build_activity(self):
        pass


class TraningCenterXml(Format):

    def __init__(self, io, user, activity_type):
        self.user = user
        self.activity_type = activity_type
        self._io = io

    def build_activity(self):
        return self._parse(self._io)

    def _parse(self, io):
        from xml.dom.minidom import parse
        self.xml_doc = parse(io)
        activity_list = self.xml_doc.getElementsByTagName('TrainingCenterDatabase')[0].getElementsByTagName('Activities')[0].childNodes
        result = []
        for node in activity_list:
            activity = self.parse_activity(node)
            result.append(activity)
        return result

    def parse_activity(self, activity_node):
        #sport = activity_node.attributes['Sport']
        id = activity_node.getElementsByTagName('Id')[0].firstChild.nodeValue
        start = dateutil.parser.parser().parse(id)
        end = None
        #activity_type = None

        activity = server.model.Activity(
            user=self.user,
            name='',
            description='',
            start=start,
            end=None,
            activity_type=self.activity_type,
            laps=[],
        )

        time = 0
        time_absolute = 0
        lap_nodes = activity_node.getElementsByTagName('Lap')
        for lap_index, lap_node in enumerate(lap_nodes):
            lap, time, time_absolute = self.parse_lap(
                lap_index,
                lap_node,
                training_time=time,
                absolute_time=time_absolute,
            )
            activity.laps.append(lap)
        activity.end = start + datetime.timedelta(seconds=activity.laps[-1].points[-1].training_time_absolute)
        return activity

    def parse_lap(self, index, lap_node, training_time, absolute_time):
        lap = server.model.Lap(index=index, points=[])

        lap.seconds = float(lap_node.getElementsByTagName('TotalTimeSeconds')[0].childNodes[0].nodeValue)
        lap.distance = float(lap_node.getElementsByTagName('DistanceMeters')[0].childNodes[0].nodeValue)
        lap.max_speed = float(lap_node.getElementsByTagName('MaximumSpeed')[0].childNodes[0].nodeValue)
        lap.calories = int(lap_node.getElementsByTagName('Calories')[0].childNodes[0].nodeValue)
        lap.avg_heart_rate = int(lap_node.getElementsByTagName('AverageHeartRateBpm')[0].childNodes[0].childNodes[0].nodeValue)
        lap.max_heart_rate = int(lap_node.getElementsByTagName('MaximumHeartRateBpm')[0].childNodes[0].childNodes[0].nodeValue)
        lap.cadence = int(lap_node.getElementsByTagName('Cadence')[0].childNodes[0].nodeValue)

        point_nodes = lap_node.getElementsByTagName('Track')[0].getElementsByTagName('Trackpoint')
        previous_time = None
        for point_index, point_node in enumerate(point_nodes):
            point, previous_time, training_time, absolute_time = self.parse_track_point(
                point_index,
                point_node,
                previous_time=previous_time,
                training_time=training_time,
                training_time_absolute=absolute_time,
            )
            lap.points.append(point)
        return lap, training_time, absolute_time

    def parse_track_point(self, index, node, training_time, training_time_absolute, previous_time=None):
        point = server.model.Point(index=index)

        time = node.getElementsByTagName('Time')[0].childNodes[0].nodeValue
        time = dateutil.parser.parser().parse(time)
        if previous_time is not None:
            delta_seconds = (time - previous_time).seconds
        else:
            delta_seconds = 0

        training_time += delta_seconds
        training_time_absolute += delta_seconds
        point.training_time = training_time
        point.training_time_absolute = training_time_absolute

        # position
        position = node.getElementsByTagName('Position')[0]
        point.lat = float(position.getElementsByTagName('LatitudeDegrees')[0].childNodes[0].nodeValue)
        point.long = float(position.getElementsByTagName('LongitudeDegrees')[0].childNodes[0].nodeValue)
        # altitude
        point.elev = int(node.getElementsByTagName('AltitudeMeters')[0].childNodes[0].nodeValue)
        # distance
        point.distance = float(node.getElementsByTagName('DistanceMeters')[0].childNodes[0].nodeValue)
        # heart rate
        point.heart_rate = int(node.getElementsByTagName('HeartRateBpm')[0].childNodes[0].childNodes[0].nodeValue)
        # cadence
        point.cadence = int(node.getElementsByTagName('Cadence')[0].childNodes[0].nodeValue)
        # speed & watts
        extensions = node.getElementsByTagName('Extensions')[0].childNodes[0]
        try:
            point.speed = float(extensions.getElementsByTagName('extv2:Speed')[0].childNodes[0].nodeValue)
        except IndexError:
            point.speed = 0.0
        try:
            point.watts = int(extensions.getElementsByTagName('extv2:Watts')[0].childNodes[0].nodeValue)
        except IndexError:
            point.watts = 0

        return point, time, training_time, training_time_absolute
