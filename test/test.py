import unittest
import datetime
import hashlib

import server.model
from server.model import User, Activity, ActivityType, Lap, Point, AccountTypes


class DBTest(unittest.TestCase):
    def test(self):
        #server.model.Database.init_db('sqlite:///memory', drop_all=True, create_all=True)
        server.model.Database.init_db('sqlite:///server.db', create_all=True, drop_all=True)

        date = datetime.datetime.now()
        with server.model.session_scope() as dbsession:
            activity_type = ActivityType(name='cycling')
            dbsession.add(activity_type)
            user = User(
                username='test',
                fullname='test test',
                email='somebody@mailserver.com',
                account_type=AccountTypes.admin
            )
            user.password = 'password'

            activity = Activity(
                user=user,
                name='activity',
                description='big trip',
                start=date,
                end=date,
                activity_type=activity_type,
                laps=[],
            )
            activity.laps.append(
                Lap(
                    index=0,
                    points=[],
                    seconds=1.0,
                    distance=2.0,
                    max_speed=32.6,
                    calories=135,
                    avg_heart_rate=120,
                    max_heart_rate=175,
                    cadence=80,
                )
            )
            activity.laps[0].points.append(Point(
                index=0, lat=1.0, long=1.0, elev=200, speed=10.5,
                distance=10.0, cadence=10, heart_rate=150, watts=90,
                training_time=1, training_time_absolute=1)
            )
            activity.laps[0].points.append(
                Point(index=1, lat=1.0, long=1.0, elev=200, speed=10.5,
                      distance=10.0, cadence=10, heart_rate=150, watts=90,
                      training_time=2, training_time_absolute=2)
            )
            dbsession.add(activity)

        with server.model.session_scope() as dbsession:
            activity_type = dbsession.query(ActivityType).filter(ActivityType.name == 'cycling').one()
            self.failUnlessEqual(activity_type.name, 'cycling')

            user = dbsession.query(User).filter(User.username == 'test').one()
            self.failUnlessEqual(user.username, 'test')
            self.failUnlessEqual(user.fullname, 'test test')
            self.failUnlessEqual(user.email, 'somebody@mailserver.com')
            self.failUnlessEqual(user.password, hashlib.sha256('password').hexdigest())


            points = dbsession.query(Point).all()
            activity = dbsession.query(Activity).one()
            self.failUnlessEqual(activity.user, user)
            self.failUnlessEqual(activity.user_id, user.id)
            self.failUnlessEqual(activity.name, 'activity')
            self.failUnlessEqual(activity.activity_type, activity_type)
            self.failUnlessEqual(activity.start, date)
            self.failUnlessEqual(activity.end, date)
            p0, p1 = activity.laps[0].points[0], activity.laps[0].points[1]
            self.failUnlessEqual(p0.id, points[0].id)
            self.failUnlessEqual(p1.id, points[1].id)


def main():
    unittest.main()

if __name__ == '__main__':
    main()