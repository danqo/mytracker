import json
import datetime
import httplib
import flask
import flask_login
import server.model
import forms

import bin.cli_client

import server.model as model
from flask_login import login_user, logout_user, current_user

from server.rest_utils import check_credentials, get_rest_client


app = flask.Flask(__name__)
app.secret_key = 'myverylongsecretkey'
login_manager = flask_login.LoginManager()
login_manager.init_app(app)

rest_client = bin.cli_client.Client()
rest_client.user = 'test'
rest_client.password = 'password'
rest_client.address = 'http://127.0.0.1:8080'


@login_manager.user_loader
def load_user(user_id):
    try:
        with server.model.session_scope() as db_session:
            user = db_session.query(model.User).get(int(user_id))
            db_session.expunge(user)
            return user
    except Exception:
        return None


@app.route('/')
def index():
    return flask.render_template('index.html', user=current_user)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = forms.LoginForm()
    if form.is_submitted():
        with model.session_scope() as db_session:
            user = db_session.query(model.User).filter_by(username=form.username.data).first()
            if not user:
                flask.abort(httplib.UNAUTHORIZED)
            if not check_credentials(user, form.password.data, plain=True):
                flask.abort(httplib.UNAUTHORIZED)
            login_user(user)
            flask.flash('Logged in successfully.')
            return flask.redirect(flask.url_for('index'))
    return flask.render_template('login.html', form=form, user=current_user)


@app.route("/logout")
@flask_login.login_required
def logout():
    logout_user()
    return flask.redirect(flask.url_for('index'))


@app.route("/register", methods=['GET', 'POST'])
def register():
    form = forms.RegisterForm()
    if form.is_submitted():

        if form.password.data != form.password_again.data:
            flask.flash('Given passwords don\'t match.')
            flask.abort(httplib.BAD_REQUEST)
        user = model.User(
            username=form.username.data,
            fullname=form.fullname.data,
            email=form.email.data,
        )
        data = user.to_dict()
        data['password'] = form.password.data
        try:
            response = rest_client.create_user(data=data)
        except Exception as e:
            print e
        if response.status_code == httplib.CREATED:
            flask.flash('Registered successfully.')
        else:
            flask.flash('User registration failed: %s' % response.content)
        return flask.redirect(flask.url_for('index'))
    return flask.render_template('register.html', form=form, user=current_user)


@app.route('/profile')
def profile():
    return flask.render_template('profile.html', user=current_user)


@app.route('/overview')
@flask_login.login_required
def overview():
    client = get_rest_client(current_user)
    response = client.user_activity(current_user.id)
    if response.ok:
        activities = json.loads(response.content)
    return flask.render_template('overview.html', user=current_user, activities=activities)


@app.route('/activities')
@flask_login.login_required
def activities():
    client = get_rest_client(current_user)
    response = client.user_activity(current_user.id)
    if response.ok:
        activities = json.loads(response.content)
    return flask.render_template('activities.html', user=current_user, activities=activities)


@app.route('/activity_detail/<activity_id>')
@flask_login.login_required
def activity_detail(activity_id):
    client = get_rest_client(current_user)
    response = client.activity_info(activity_id=activity_id)
    if response.ok:
        activity = json.loads(response.content)
    return flask.render_template('activity_detail.html', user=current_user, activity=activity)


@app.route('/import', methods=['GET', 'POST'])
@flask_login.login_required
def import_activity():
    form = forms.ImportActivityForm()
    if form.is_submitted():
        filename = form.file.data.filename
        form.file.data.save('/tmp/' + filename)
        #io = StringIO.StringIO(form.file.data)
        client = get_rest_client(current_user)
        import server.formats
        with server.model.session_scope() as db_session:
            activity_type = db_session.query(server.model.ActivityType).get(1)
            with open('/tmp/' + filename, 'r') as io:
                file = server.formats.TraningCenterXml(io, activity_type=activity_type, user=current_user)
                activities = file.build_activity()
                for activity in activities:
                    activity.name = "imported activity"
                    activity.description = "Activity imported on" + str(datetime.datetime.now())
                    client.create_activity(activity.to_dict())
        return flask.redirect(flask.url_for('activities'))

    return flask.render_template('import_activity.html', user=current_user, form=form)

