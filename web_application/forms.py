import wtforms
import flask_wtf


class LoginForm(flask_wtf.FlaskForm):
    username = wtforms.StringField('Username')
    password = wtforms.PasswordField('Password')


class RegisterForm(flask_wtf.FlaskForm):
    username = wtforms.StringField('Username')
    fullname = wtforms.StringField('fullname')
    email = wtforms.StringField('Email')
    password = wtforms.PasswordField('Password')
    password_again = wtforms.PasswordField('Password again')


class ImportActivityForm(flask_wtf.FlaskForm):
    file = wtforms.FileField()
