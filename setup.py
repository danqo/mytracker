from setuptools import setup

setup(
    name='MyTracker',
    version='1.0',
    author='Daniel Jackuliak',
    author_email='daniel.jackuliak@gmail.com',
    include_package_data=True,
    packages=[
        'bin',
        'server',
        'web_application',
        'test',
    ],
    requires=['requests', 'flask', 'sqlalchemy'],
)
