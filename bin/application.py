import argparse

class Application(object):

    def __init__(self, standalone=False, prog=None, usage=None, description=None):
        self.parser = None
        if standalone:
            self.parser = argparse.ArgumentParser(
                prog=prog,
                usage=usage,
                description=description
            )
            self._init_arg_parser()

    def main(self):
        raise NotImplemented('main method not implemented')

    def _init_arg_parser(self):
        raise NotImplemented('_init_parser')