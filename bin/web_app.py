import application
import server
import server.model
import web_application


class WebApp(application.Application):
    CONFIG = 'server.ini'
    DB_PATH = 'server.db'

    def __init__(self, standalone=True, prog=None, usage=None, description=None):
        super(WebApp, self).__init__(
            standalone=standalone,
            prog=prog,
            usage=usage,
            description=description
        )
        self.db_path = None

    def _init_arg_parser(self):
        self.parser.add_argument('-p', '--path', default=WebApp.DB_PATH)

    def run_flask(self, workers=1):
        server.model.Database.init_db(self.db_path)

        # with open('/home/daniel/workspace/mytracker/samples/sample1.tcx', 'r') as data:
        #     with server.model.session_scope() as db_session:
        #         user = db_session.query(server.model.User).get(1)
        #         activity_type = db_session.query(server.model.ActivityType).get(1)
        #         tcx_file = server.formats.TraningCenterXml(user=user, activity_type=activity_type, io=data)

        web_application.web_rest.app.run(host='127.0.0.1', port=8081, processes=workers)

    def main(self):
        args = self.parser.parse_args()
        db_path = args.path
        self.db_path = "sqlite:///" + db_path
        self.run_flask()


def main():
    web_app = WebApp()
    web_app.main()


if __name__ == '__main__':
    main()
