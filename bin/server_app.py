import application
import ConfigParser
import server

class Server(application.Application):

    CONFIG = 'server.ini'
    DB_PATH = 'server.db'

    def __init__(self, standalone=True, db_path=None, prog=None, usage=None, description=None):
        super(Server, self).__init__(
            standalone=standalone,
            prog=prog,
            usage=usage,
            description=description
        )
        self.db_path = db_path
        #config_parser = ConfigParser.ConfigParser()
        #self.config = config_parser.read(Server.CONFIG)

    def _init_arg_parser(self):
        self.parser.add_argument('-p', '--path', default=Server.DB_PATH)
        subparsers = self.parser.add_subparsers(help='sub-command help', dest='subparser_name')
        parser_init = subparsers.add_parser('init', help="Init command help")
        #parser_init.add_argument('-p', '--path', default=Server.DB_PATH)

        subparsers.add_parser('simple_run', help="Run simple WSGI REST based server.")

        flask_run = subparsers.add_parser('flask_run', help="Run flask server")
        flask_run.add_argument('-w', '--workers', type=int, default=1)

    def init_server(self):
        print "init server %s" % self.db_path
        server.model.Database.init_db(self.db_path, create_all=True)

    def run_flask(self, workers=1):
        print "run flask"
        server.model.Database.init_db(self.db_path)
        server.flask_rest.app.run(host='127.0.0.1', port=8080, processes=workers)

    def run_simple(self):
        server.model.Database.init_db(self.db_path)

        dispatcher = server.simple_rest.PathDispatcher()
        dispatcher.register('GET', '/ping', server.simple_rest.ping)
        dispatcher.register('GET', '/users', server.simple_rest.users)

        from wsgiref.simple_server import make_server
        httpd = make_server('', 8080, dispatcher)
        print('Serving on 8080...')
        httpd.serve_forever()

    def main(self):
        args = self.parser.parse_args()
        db_path = args.path or Server.DB_PATH
        self.db_path = "sqlite:///" + db_path
        if args.subparser_name == 'init':
            self.init_server()
        elif args.subparser_name == 'simple_run':
            self.run_simple()
        elif args.subparser_name == 'flask_run':
            self.run_flask(args.workers)
        elif args.subparser_name == 'help':
            self.parser.print_help()
        else:
            self.parser.print_help()


def main():
    server = Server()
    server.main()

if __name__ == '__main__':
    main()
