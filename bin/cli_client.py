import requests
import application
import json
import contextlib


class RequestMethod(object):
    GET = 'get'
    POST = 'post'
    PUT = 'put'
    DELETE = 'delete'


class Client(application.Application):

    def __init__(self, standalone=False):
        super(Client, self).__init__(standalone=standalone)
        # TODO: encapslate by HTTPauthorization
        self.user, self.password, self.address = (None, None, None)
        if standalone:
            self.args = self.parser.parse_args()
            self.user = self.args.user
            self.password = self.args.password
            self.address = self.args.address

    def _init_arg_parser(self):
        self.parser.add_argument('-u', '--user')
        self.parser.add_argument('-p', '--password')
        self.parser.add_argument('-a', '--address', default='http://127.0.0.1:8080')
        subparsers = self.parser.add_subparsers(help='sub-command help', dest='subparser_name')

        subparsers.add_parser('catalog', help="List all available operations")
        subparsers.add_parser('user-list', help="List all users")
        subparsers.add_parser('activity-types-list', help="List all activity types")

        user_info = subparsers.add_parser('user-info', help="Get user info")
        user_info.add_argument('user_name')

        create_user = subparsers.add_parser('user-create', help='Create new user')
        create_user.add_argument('data')

        update_user = subparsers.add_parser('user-update', help='Update user')
        update_user.add_argument('user_id')
        update_user.add_argument('data')

        delete_user = subparsers.add_parser('user-delete', help='Delete user')
        delete_user.add_argument('user_id')

        # activity
        activity_info = subparsers.add_parser('user-activity', help="Get activities of given user")
        activity_info.add_argument('user_id')

        activity_info = subparsers.add_parser('activity-info', help="Get activity info")
        activity_info.add_argument('activity_id')

        create_activity = subparsers.add_parser('activity-create', help='Create new activity')
        create_activity.add_argument('data')

        update_activity = subparsers.add_parser('activity-update', help='Update activity')
        update_activity.add_argument('activity_id')
        update_activity.add_argument('data')

        delete_activity = subparsers.add_parser('activity-delete', help='Delete activity')
        delete_activity.add_argument('activity_id')

    def send_request(self, method, url, **kwargs):
        try:
            response = requests.request(
                auth=(self.user, self.password),
                method=method,
                url=url,
                **kwargs
            )
            return response
        except requests.RequestException as re:
            print "Unable to sent request: %s" % re.message

    def main(self):

        response = None
        if self.args.subparser_name == 'catalog':
            pass
        elif self.args.subparser_name == 'user-list':
            response = self.user_list()
        elif self.args.subparser_name == 'activity-types-list':
            response = self.activity_types_list()
        elif self.args.subparser_name == 'user-info':
            response = self.user_info(
                user_name=self.args.user_name
            )
        elif self.args.subparser_name == 'user-create':
            response = self.create_user(
                data=json.loads(self.args.data),
            )
        elif self.args.subparser_name == 'user-update':
            response = self.update_user(
                user_id=self.args.user_id,
                data=json.loads(self.args.data),
            )
        elif self.args.subparser_name == 'user-delete':
            response = self.delete_user(
                user_id=self.args.user_id
            )
        elif self.args.subparser_name == 'user-activity':
            response = self.user_activity(
                user_id=self.args.user_id
            )
        elif self.args.subparser_name == 'activity-info':
            response = self.activity_info(
                activity_id=self.args.activity_id
            )
        elif self.args.subparser_name == 'activity-create':
            response = self.create_activity(
                data=json.loads(self.args.data),
            )
        elif self.args.subparser_name == 'activity-update':
            response = self.update_activity(
                activity_id=self.args.activity_id,
                data=json.loads(self.args.data),
            )
        elif self.args.subparser_name == 'activity-delete':
            response = self.delete_activity(
                activity_id=self.args.activity_id,
            )
        elif self.args.subparser_name == 'help':
            self.parser.print_help()
        else:
            self.parser.print_help()
        if response:
            print "Response: %s" % response
            print "Data: %s" % response.text

    def activity_types_list(self):
        return self.send_request(
            method=RequestMethod.GET,
            url=self.address + '/api/activity_types'
        )

    def user_list(self):
        return self.send_request(
            method=RequestMethod.GET,
            url=self.address + '/api/users'
        )

    def user_info(self, user_name):
        return self.send_request(
            method=RequestMethod.GET,
            url=self.address + '/api/users/%s' % user_name,
        )

    def create_user(self, data):
        return self.send_request(
            method=RequestMethod.PUT,
            url=self.address + '/api/users',
            json=data,
        )

    def update_user(self, user_id, data):
        return self.send_request(
            method=RequestMethod.POST,
            url=self.address + '/api/users/%s' % user_id,
            json=data,
        )

    def delete_user(self, user_id):
        return self.send_request(
            method=RequestMethod.DELETE,
            url=self.address + '/api/users/%s' % user_id,
        )

    def user_activity(self, user_id):
        return self.send_request(
            method=RequestMethod.GET,
            url=self.address + '/api/user_activity/%s' % user_id,
        )

    def activity_info(self, activity_id):
        return self.send_request(
            method=RequestMethod.GET,
            url=self.address + '/api/activity/%s' % activity_id,
        )

    def create_activity(self, data):
        return self.send_request(
            method=RequestMethod.PUT,
            url=self.address + '/api/activity/',
            json=data
        )

    def update_activity(self, activity_id, data):
        return self.send_request(
            method=RequestMethod.POST,
            url=self.address + '/api/activity/%s' % activity_id,
            json=data
        )

    def delete_activity(self, activity_id):
        return self.send_request(
            method=RequestMethod.DELETE,
            url=self.address + '/api/activity/%s' % activity_id,
        )


def main():
    client = Client(standalone=True)
    client.main()


if __name__ == '__main__':
    main()
